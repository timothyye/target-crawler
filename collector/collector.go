package collector

import (
	"fmt"
	"log"
	"net/url"
	"strings"

	"github.com/PuerkitoBio/goquery"
	"github.com/fatih/color"
)

// URLCollector struct, with base URL and url dictionary
type URLCollector struct {
	Base string
	host string
	base *url.URL
	urls map[string]string
}

// PrintURLs output URLs and sitemap
func (c *URLCollector) PrintURLs() {
	color.Magenta("Sitemap for %s", c.host)
	fmt.Println("=================================")
	for u, v := range c.urls {
		color.Green("[%s] - [%s] \r\n", v, u)
	}
}

// NewURLCollector returns a new collector
func NewURLCollector(targetURL, hostURL string) *URLCollector {
	collector := &URLCollector{Base: targetURL, host: hostURL}
	return collector
}

// StartParse func walkthrough all link elements and collect valid ones
func (c *URLCollector) StartParse(targetURL string) {

	if targetURL == "" {
		targetURL = c.Base
	}
	doc, err := goquery.NewDocument(targetURL)
	if err != nil {
		log.Fatal(err)
	}

	doc.Find("a").Each(func(_ int, s *goquery.Selection) {
		u, exists := s.Attr("href")
		if !exists {
			return
		}

		title := s.Text()
		if strings.TrimSpace(title) == "" {
			return
		}

		if strings.HasPrefix(u, "#") || strings.Contains(u, ".pdf") {
			return
		}

		if c.base == nil {
			var err error
			if c.base, err = url.Parse(c.Base); err != nil {
				return
			}
		}

		urlobj, err := c.base.Parse(u)
		if err != nil {
			return
		}

		if urlobj.Scheme != "http" && urlobj.Scheme != "https" {
			return
		}

		// Filter out the external URLs
		if urlobj.Host != c.host {
			return
		}

		if c.urls == nil {
			c.urls = make(map[string]string)
		}

		// Only save new URLs
		if _, exists := c.urls[urlobj.String()]; !exists {
			c.urls[urlobj.String()] = title
			fmt.Println("Start to parse:", urlobj.String())
			// Regression for the new URL
			c.StartParse(urlobj.String())
		} else {
			return
		}
	})
}
