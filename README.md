# Target-Crawler
A simple crawler written in Go, it simply walkthrough [http://www.target-energysolutions.com/](http://www.target-energysolutions.com/) and prints a simple sitemap.

## Get and build it
```bash
git clone git@bitbucket.org:timothyye/target-crawler.git
go get -v
go build
```

## Run it
```bash
./target-crawler
```

## Demo

![](https://bitbucket.org/timothyye/target-crawler/raw/08ba60ee9ed683f55c06343587060a64bcb88b1c/demo/target.png)