package main

import "target-crawler/collector"

func main() {
	c := collector.NewURLCollector("http://www.target-energysolutions.com/", "www.target-energysolutions.com")
	c.StartParse("")
	c.PrintURLs()
}
